# Script:  caesar.py
# Desc:    encrypt and decrypt text with a Caesar cipher
#          using defined character set with index
# Author:  Ross Macleod, Mia Bell, Colin Moulds
# Created: 02/10/17
# note that you should add a module doc string!

import os
import sys, math

charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"  # characters to be encrypted
numchars = len(charset)  # number of characters, for wrapping round


def entropy():
    """prints the entropy value of the entered ascii password"""
    print("")
    passwd = input('[*] Enter password : ')
    char_set = 95
    passwdPerHour = int(input('[*] Enter Passwords per hour : '))
    keyspace = char_set**len(passwd)
    AvAttempts = keyspace/2
    AvCrackTime = AvAttempts/passwdPerHour
    print('[*] Total of : ', keyspace, ' key combinations')
    print('[*] Average Attempts : ', keyspace/2)
    print('[*] Average time to crack : ', AvCrackTime, ' Hours')

def caesar_encrypt(plaintext, key):
    """put an appropriate function doc string here"""
    print("")
    print (f'[*] ENCRYPTING - key: {key}; plaintext: {plaintext}')
    plaintext = plaintext.upper()     # convert plaintext to upper case
    ciphertext = ''  # initialise ciphertext as empty string
    for ch in plaintext:
        if ch in charset:
            new = ch
        else:
            new = ''  # do nothing with characters not in charset
        ciphertext = ciphertext+new
    cipher = list(ciphertext)  # converts ciphertext into a list
    for x in range(len(cipher)):  # loops for each char in cipher list
        for y in range(len(charset)):  # loops for each char in charset
            if cipher[x] == charset[y]:  # compares cipher char to charset char
                z = int(y + key)  # uses the charset position and key to get correct shift
                if z > 25:  # verifies the new char is within limits
                    z = z - 25  # if out of limits corrects by starting from 0
                cipher[x] = charset[z]  # sets the char in cipher to the new encrypted char
                break  # starts the process again for the next char in cipher
            z = 0     # resets z for the next loop
    ciphertext = ''.join(cipher)    # converts the list into a more readable format
    print("")
    print (f'[*] ciphertext: {ciphertext}')  # prints the new encrypted text
    return ciphertext  # returns ciphertext so it can be reused


def caesar_decrypt(ciphertext, key):
    """put an appropriate function doc string here"""
    # very similar to caesar_encrypt(), but shift left
    print("")
    print (f'[*] DECRYPTING - key: {key}; ciphertext: {ciphertext}')
    key = key * - 1
    ciphertext = ciphertext.upper()  # convert plaintext to upper case
    plaintext = ''  # initialise ciphertext as empty string

    for ch in ciphertext:
        if ch in charset:
            new = ch
        else:
            new = ''  # do nothing with characters not in charset
        plaintext = plaintext+new
    plain = list(plaintext)
    for x in range(len(plain)):
        for y in range(len(charset)):
            if plain[x] == charset[y]:
                z = int(y + key)
                if z > 25:
                    z = z - 25
                plain[x] = charset[z]
                break
            z = 0
    plaintext = ''.join(plain)
    print("")
    print (f'[*] plaintext: {plaintext}')
    return plaintext  # returns plaintext so it can be reused


def caesar_crack(ciphertext):
    """put an appropriate function doc string here"""
    for key in range(26):
        cipher = list(ciphertext)
        for x in range(len(cipher)):
            for y in range(len(charset)):
                if cipher[x] == charset[y]:
                    z = int(y - key)
                    cipher[x] = charset[z]
                    break
                z = 0
        ciphertext = ''.join(cipher)
        print("|")
        print (f'[*] crack #{key}: {ciphertext}')


def Menu():
    print("#######################")
    print("#    Caesar Cipher    #")
    print("#       Program       #")
    print("#######################")
    print("")
    print("- 1. Encrypt")
    print("- 2. Decrypt")
    print("- 3. Cracker")
    print("- 4. Entropy Checker")
    print("- 5. Exit")
    print("-----------------------")
    choice = input("Enter Choice - ")
    return int(choice)


def main():
    os.system('CLS')
    choice = Menu()
    # test cases
    key = 2
    plain1 = 'Hello Suzanne'
    cipher1 = 'IQQfOQtpKpIGXGtaQPG'
    crackme = 'PBATENGHYNGVBAFLBHUNIRPENPXRQGURPBQRNAQGURFUVSGJNFGUVEGRRA'
    # call functions with text cases
    if choice == 1:
        caesar_encrypt(plain1, key)
    elif choice == 2:
        caesar_decrypt(cipher1, key)
    elif choice == 3:
        caesar_crack(crackme)
    elif choice == 4:
        entropy()
    elif choice == 5:
        exit
    else:
        print("Invalid Input")
    print("")
    input("press enter to return to menu...")
# boilerplate
if __name__ == '__main__':
    while True:
        main()
